export interface BracketPair {
	readonly l: string;
	readonly r: string;
	readonly active: boolean;
}
